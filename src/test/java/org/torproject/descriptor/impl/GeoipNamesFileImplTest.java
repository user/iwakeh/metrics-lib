package org.torproject.descriptor.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.torproject.descriptor.DescriptorParseException;
import org.torproject.descriptor.GeoipNamesFile;

import org.apache.commons.compress.utils.IOUtils;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.SortedMap;
import java.util.TreeMap;


public class GeoipNamesFileImplTest {

  @Test
  public void testParseGeoipAsnNames()
          throws IOException, DescriptorParseException {
    SortedMap<String, String> expectedResults = new TreeMap<>();
    expectedResults.put("7", "The Defence Science and Technology Laboratory");
    expectedResults.put("28",
            "Deutsches Zentrum fuer Luft- und Raumfahrt e.V.");
    expectedResults.put("137", "Consortium GARR");
    expectedResults.put("224", "UNINETT AS");
    expectedResults.put("251", "Denis Klimek");
    expectedResults.put("328838", "AIM Firms Limited");
    expectedResults.put("328839", "Silver Solutions 1234");
    URL resource = getClass().getClassLoader().getResource(
            "geoip/asn.txt");
    assertNotNull(resource);
    InputStream dataInputStream = resource.openStream();
    assertNotNull(dataInputStream);
    byte[] rawDescriptorBytes = IOUtils.toByteArray(dataInputStream);
    GeoipNamesFile geoipNamesFile = new GeoipNamesFileImpl(rawDescriptorBytes,
            new int[]{0, rawDescriptorBytes.length}, null);
    assertFalse(geoipNamesFile.isEmpty());
    for (String k : expectedResults.keySet()) {
      assertEquals(expectedResults.get(k), geoipNamesFile.get(k));
    }
    assertTrue(geoipNamesFile.getUnrecognizedLines() == null
            || geoipNamesFile.getUnrecognizedLines().isEmpty());
  }


  @Test
  public void testParseGeoipCountryNames()
          throws IOException, DescriptorParseException {
    SortedMap<String, String> expectedResults = new TreeMap<>();
    expectedResults.put("AD", "Andorra");
    expectedResults.put("AE", "United Arab Emirates");
    expectedResults.put("BQ", "Bonaire, Sint Eustatius and Saba");
    expectedResults.put("ZW", "Zimbabwe");
    URL resource = getClass().getClassLoader().getResource(
            "geoip/countries.txt");
    assertNotNull(resource);
    InputStream dataInputStream = resource.openStream();
    assertNotNull(dataInputStream);
    byte[] rawDescriptorBytes = IOUtils.toByteArray(dataInputStream);
    GeoipNamesFile geoipNamesFile = new GeoipNamesFileImpl(rawDescriptorBytes,
            new int[]{0, rawDescriptorBytes.length}, null);
    assertFalse(geoipNamesFile.isEmpty());
    for (String k : expectedResults.keySet()) {
      assertEquals(expectedResults.get(k), geoipNamesFile.get(k));
    }
    assertTrue(geoipNamesFile.getUnrecognizedLines() == null
            || geoipNamesFile.getUnrecognizedLines().isEmpty());
  }
}
